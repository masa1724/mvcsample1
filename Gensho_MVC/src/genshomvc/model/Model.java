package genshomvc.model;

import genshomvc.Constants;
import genshomvc.observer.Subject;

public class Model extends Subject<ModelData> {
	//--------------------------------------------------
	// データ
	//--------------------------------------------------
	private ModelData data = new ModelData(Constants.RANK_CODE_POOR);
	
	public ModelData getModelData() {
		return data;
	}
	
	//--------------------------------------------------
	// ビジネスロジック
	//--------------------------------------------------
	public void increment() {
		data.setCounter(data.getCounter() + 1);
		data.setRankCode(getRankCode(data.getCounter()));
		notifyObservers(this.data); // 変更を通知
	}
	public void decrement() {
		if (data.getCounter() == 0) return; // 下限を0とする
		data.setCounter(data.getCounter() - 1);
		data.setRankCode(getRankCode(data.getCounter()));
		notifyObservers(this.data); // 変更を通知
	}
	
	private int getRankCode(int counter) {
		int rankCode = Constants.RANK_CODE_POOR;
		
		if (0 <= counter && counter <= 2) {
			rankCode = Constants.RANK_CODE_POOR;
		} else if (3 <= counter && counter <= 5) {
			rankCode = Constants.RANK_CODE_AVERAGE;
		} else if (6 <= counter && counter <= 8) {
			rankCode = Constants.RANK_CODE_GOOD;
		} else if (9 <= counter && counter <= 12) {
			rankCode = Constants.RANK_CODE_VERYGOOD;
		} else if (counter >= 13) {
			rankCode = Constants.RANK_CODE_EXCELLENT;
		}
		// elseは該当しえない
		
		return rankCode;
	}
}
