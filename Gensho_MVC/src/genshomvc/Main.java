package genshomvc;

import java.util.Scanner;

import genshomvc.model.Model;

/**
 * メインプロセス
 */
public class Main {
	public static void main(String[] args) {
		System.out.println("■■■　原初MVC模倣 ■■■");

		Controller c = new Controller();
		Model m = new Model();
		View v = new View(m.getModelData());
		m.addObserver(v);

		try (Scanner scanner = new Scanner(System.in)) {
			while(true) {
				// キーボード入力のイベント発生
				String inputValue = scanner.next();
				if (inputValue.equals("exit")) break; // 停止用
				UIKeyDownEvents e = new UIKeyDownEvents(inputValue);
				
				// Controllerでイベントを受け取り、内部でModelを起動
				c.handle(e, m);
				
				// 表示内容を更新
				System.out.println(v.render());
			}
		}
	}
}
