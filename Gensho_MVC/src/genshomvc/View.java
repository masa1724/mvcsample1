package genshomvc;

import genshomvc.model.ModelData;
import genshomvc.observer.Observer;

public class View implements Observer<ModelData> {
	private ModelData refModelData;
	
	// ヌルポ対策
	public View (ModelData modelData) {
		this.refModelData = modelData;
	}

	// Modelを参照
	@Override
	public void update(ModelData modelData) {
		refModelData = modelData;
	}
	
	// 描画
	public String render() {
		return "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■\r\n"
				+ "Counter:" + refModelData.getCounter() + "\r\n"
				+ "現在のランク:" + getRankName(refModelData.getRankCode()) + "\r\n"
				+ "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■";
	}
	
	private String getRankName(int rankCode) {
		String rankName = null;
		
		switch (rankCode) {
		case Constants.RANK_CODE_POOR:
			rankName = "良くない";
			break;
		case Constants.RANK_CODE_AVERAGE:
			rankName = "あまり良くない";
			break;
		case Constants.RANK_CODE_GOOD:
			rankName = "良い";
			break;
		case Constants.RANK_CODE_VERYGOOD:
			rankName = "とても良い";
			break;
		case Constants.RANK_CODE_EXCELLENT:
			rankName = "素晴らしい";
			break;
		default:
			// 本来該当しえない
			throw new IllegalArgumentException("不正なランクコードが指定されています。");
		}
		
		return rankName;
	}
}
