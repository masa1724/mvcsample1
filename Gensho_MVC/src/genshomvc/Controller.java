package genshomvc;

import genshomvc.model.Model;

public class Controller {
	public void handle(UIKeyDownEvents e, Model m) {
		String inputValue = e.getInputValue();
		
		// UIイベントに合わせて、ビジネスロジックを実行
		if (inputValue.equals("+")) {
			m.increment();
			
		} else if(inputValue.equals("-")) {
			m.decrement();
		}
		
		// +-以外は何もしない
	}
}
