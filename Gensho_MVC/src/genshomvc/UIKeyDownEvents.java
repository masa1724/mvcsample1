package genshomvc;

/**
 * UIのキーボード入力操作を表現したイベントクラス
 *
 */
public class UIKeyDownEvents {
	private String inputValue;

	public UIKeyDownEvents(String inputValue) {
		this.inputValue = inputValue;
	}

	public String getInputValue() {
		return inputValue;
	}
}
