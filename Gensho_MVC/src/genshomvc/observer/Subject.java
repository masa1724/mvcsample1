package genshomvc.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * �ώ@�Ώێ҂��Ǘ�
 *
 * @param <T> �ώ@�Ώۃf�[�^�̌^�p�����[�^
 */
public abstract class Subject<T> {
    private List<Observer<T>> observers = new ArrayList<>();

    public void addObserver(Observer<T> observer) {
        observers.add(observer);
    }
    public void deleteObserver(Observer<T> observer) {
        observers.remove(observer);
    }
    public void notifyObservers(T obj) {
        for (Observer<T> observer : observers) {
            observer.update(obj);
        }
    }
}
