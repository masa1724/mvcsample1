package mvc2;

import java.util.Scanner;

import mvc2.model.Model;

/**
 * メインプロセス
 */
public class Main {
	public static void main(String[] args) {
		System.out.println("■■■　MVC2模倣 ■■■");

		Controller c = new Controller();
		Model m = new Model();
		View v = new View();

		try (Scanner scanner = new Scanner(System.in)) {
			while(true) {
				// キーボードのイベント発生
				String inputValue = scanner.next();
				if (inputValue.equals("exit")) break; // 停止用
				UIKeyDownEvents e = new UIKeyDownEvents(inputValue);
				
				// Controllerでイベントを受け取り、内部でModelを起動し戻り値を取得
				// View-Model間を分断するため、戻り値はModelではなく画面表示用のパラメータクラスとする
				ViewParameter parameter = c.handle(e, m);
				
				// パラメータを基に表示内容を更新
				System.out.println(v.render(parameter));
			}
		}
	}
}
