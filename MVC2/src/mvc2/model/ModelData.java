package mvc2.model;

public class ModelData {
	private int counter;
	private int rankCode;
	
	// パッケージレベルでの公開としModelからのみインスタンス生成可能とする
	protected ModelData(int rankCode) {
		this.counter = 0;
		this.rankCode = rankCode;
	}
	
	// パッケージレベルでの公開としModelからのみ変更可能とする。
	protected void setCounter(int newCounter) {
		this.counter = newCounter;
	}
	protected void setRankCode(int rankCode) {
		this.rankCode = rankCode;
	}
	
	public int getCounter() {
		return counter;
	}
	public int getRankCode() {
		return rankCode;
	}
}
