package mvc2.model;

import mvc2.Constants;

public class Model {
	//--------------------------------------------------
	// データ
	//--------------------------------------------------
	private ModelData data = new ModelData(Constants.RANK_CODE_POOR);
	
	public ModelData getModelData() {
		return data;
	}
	
	//--------------------------------------------------
	// ビジネスロジック
	//--------------------------------------------------
	public ModelData increment() {
		data.setCounter(data.getCounter() + 1);
		data.setRankCode(getRankCode(data.getCounter()));
		return data;
	}
	public ModelData decrement() {
		if (data.getCounter() == 0) return data; // 下限を0とする
		data.setCounter(data.getCounter() - 1);
		data.setRankCode(getRankCode(data.getCounter()));
		return data;
	}
	
	private int getRankCode(int counter) {
		int rankCode = Constants.RANK_CODE_POOR;
		
		if (0 <= counter && counter <= 2) {
			rankCode = Constants.RANK_CODE_POOR;
		} else if (3 <= counter && counter <= 5) {
			rankCode = Constants.RANK_CODE_AVERAGE;
		} else if (6 <= counter && counter <= 8) {
			rankCode = Constants.RANK_CODE_GOOD;
		} else if (9 <= counter && counter <= 12) {
			rankCode = Constants.RANK_CODE_VERYGOOD;
		} else if (counter >= 13) {
			rankCode = Constants.RANK_CODE_EXCELLENT;
		}
		// elseは該当しえない
		
		return rankCode;
	}
}
