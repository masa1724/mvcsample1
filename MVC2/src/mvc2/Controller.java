package mvc2;

import mvc2.model.Model;
import mvc2.model.ModelData;

public class Controller {
	public ViewParameter handle(UIKeyDownEvents e, Model m) {
		String inputValue = e.getInputValue();
		
		ModelData data = m.getModelData();
		
		// UIイベントに合わせて、ビジネスロジックを実行しその戻り値を取得
		if (inputValue.equals("+")) {
			data = m.increment();
			
		} else if(inputValue.equals("-")) {
			data = m.decrement();
		}
		
		return new ViewParameter(data.getCounter(), getRankName(data.getRankCode()));
	}
	
	private String getRankName(int rankCode) {
		String rankName = null;
		
		switch (rankCode) {
		case Constants.RANK_CODE_POOR:
			rankName = "良くない";
			break;
		case Constants.RANK_CODE_AVERAGE:
			rankName = "あまり良くない";
			break;
		case Constants.RANK_CODE_GOOD:
			rankName = "良い";
			break;
		case Constants.RANK_CODE_VERYGOOD:
			rankName = "とても良い";
			break;
		case Constants.RANK_CODE_EXCELLENT:
			rankName = "素晴らしい";
			break;
		default:
			// 本来該当しえない
			throw new IllegalArgumentException("不正なランクコードが指定されています。");
		}
		
		return rankName;
	}
}
