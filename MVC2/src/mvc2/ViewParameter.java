package mvc2;

public class ViewParameter {
	private int counter;
	private String rankName;
	
	public ViewParameter(int counter, String rankName) {
		this.counter = counter;
		this.rankName = rankName;
	}
	
	public int getCounter() {
		return counter;
	}
	public String getRankName() {
		return rankName;
	}
}
