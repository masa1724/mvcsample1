package mvc2;

public class Constants {
	public static final int RANK_CODE_EXCELLENT = 5;
	public static final int RANK_CODE_VERYGOOD = 4;
	public static final int RANK_CODE_GOOD = 3;
	public static final int RANK_CODE_AVERAGE = 2;
	public static final int RANK_CODE_POOR = 1;
}
